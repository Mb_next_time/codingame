#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <cmath>

using namespace std;

string change(string str)
{
    for(int k = 0; k < str.size(); k++) 
        if(str[k] == ',')
        {   
            str[k] = '.';
            break;
        }
    return str;
}

int main()
{   
    const double pi = 3.1415926536;
    int count = 0;
    int count_lim = 0;
    string LON;
    cin >> LON; cin.ignore();
    string LAT;
    cin >> LAT; cin.ignore();
    int N;
    cin >> N; cin.ignore();
    string buf;
    string place;
    string point;
    int flag_beg = 0;
    double LON_1;
    double LAT_1;
    double LON_2;
    double LAT_2;
    double min = 10000;
    LON = change(LON);
    LON_1 = atof(LON.c_str())*pi/180;
    LAT = change(LAT);
    LAT_1 = atof(change(LAT).c_str())*pi/180;
    for (int i = 0; i < N; i++) {
        string DEFIB;
        getline(cin, DEFIB);
        for(int j = 1; j < DEFIB.size(); j++) {
            if(DEFIB[j] == ';')
            {   
                count++;
                if(count == 1)
                {   
                    flag_beg = j+1;
                    for(j = flag_beg; DEFIB[j] != ';';j++) 
                        count_lim++;
                    place = DEFIB.substr(flag_beg, count_lim);
                    count_lim = 0;
                    
                    count++;
                }
                if(count == 4)
                {   
                    
                    flag_beg = j+1;
                    for(j = flag_beg; (DEFIB[j] != ';');j++) 
                        count_lim++;
                    buf = change(DEFIB.substr(flag_beg, count_lim));
                    count_lim = 0;
                    
                    LON_2 = atof(buf.c_str())*pi/180;
                    
                    
                    flag_beg = j+1; 
                    for(j = flag_beg; j < DEFIB.size();j++) 
                        count_lim++;
                    buf = change(DEFIB.substr(flag_beg, count_lim));
                    count_lim = 0;
                    
                    LAT_2 = atof(buf.c_str())*pi/180;
                    count = 0;
                    
                    double x,y,d;
                    x = (LON_2 - LON_1)*cos((LAT_1+LAT_2)/2);
                    y = LAT_2 - LAT_1;
                    d = sqrt(x*x+y*y)*6371;
                    if(d < min)
                    {
                        min = d;
                        point = place;
                    }
                }
            }
        }
    }
    cout << point << endl;
}