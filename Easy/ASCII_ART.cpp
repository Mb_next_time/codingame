#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
 
int main() {
    
    int L;
    cin >> L; cin.ignore();
    int H;
    cin >> H; cin.ignore();
    string T;
    getline(cin, T);
 
    for (int i = 0; i < H; i++)  {
        string ROW;
        getline(cin, ROW);
        int k = 0;
        while(T[k] != '\0') {   
            
            if((T[k] >= 65) && (T[k] <= 90)) {
                for(int j = ((int)T[k]-65)*L; j < ((int)T[k]-65)*L+L; j++)
                    cout << ROW[j];
                k++;
            }
            else {
                if((T[k] >= 97) && (T[k] <= 120)) {
                    for(int j = ((int)T[k]-97)*L; j < ((int)T[k]-97)*L+L; j++)
                        cout << ROW[j];
                    k++;
                }
                else {
                    if(T[k] != '\0') {
                        for(int j = 104; j < 108; j++)
                            cout << ROW[j];
                        k++;   
                    }
                }
            }
        }
        cout << endl;
    }
}