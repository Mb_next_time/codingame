#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
    int N;
    cin >> N; cin.ignore();
    vector<int> v(N);
    
    for (int i = 0; i < N; i++) {
        int Pi;
        cin >> Pi; cin.ignore();
        v[i] = Pi;
    }
    
    sort(v.begin(), v.begin()+N);
    

    int diff = 9999999;
    for (int i = 0; i < N-1; i++) {
        if( diff > (v[i+1] - v[i])) {
            diff = v[i+1] - v[i];
        }
    }
    cout << diff << endl;
}