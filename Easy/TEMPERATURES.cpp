#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <cmath>

using namespace std;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
int main()
{
    int n; // the number of temperatures to analyse
    cin >> n; cin.ignore();
    int count_lim = 1;
    int minus;
    int min = 5526;
    int min_n = 5526;
    int min_p = 5526;
    int flag_beg;
    string buf;
    string temps; // the n temperatures expressed as integers ranging from -273 to 5526
    getline(cin, temps);
    int *mas_of_temp = new int[n];
    int j = 0;
    if(n == 0) {
        min = 0;
    }
    else {
        for(int i = 0; i < temps.size(); i++) {
            if( temps[i] == '-' ) {
                minus = 1;
            }
            if( (temps[i] >= '0') && (temps[i] <= '9') ) {
                flag_beg = i;
                while(temps[i+1] != ' ') {
                    count_lim++;
                    i++;
                    if(temps[i+1] == '\0') {
                        break;
                    }
                }
                buf = temps.substr( flag_beg, count_lim );
                int x;
                if( minus == 1 ) {
                    x = -atoi( buf.c_str() );
                    mas_of_temp[j] = x;
                    j++;
                }
                else {
                    x = atoi( buf.c_str() );
                    mas_of_temp[j] = x;
                    j++;
                }
                minus = 0;
                count_lim = 1;
            }    
        }
       
        for(int j = 0; j < n; j++) {
            if(mas_of_temp[j] < 0) {
                if( abs(mas_of_temp[j]) < min_n) {
                    min_n = abs(mas_of_temp[j]);
                }
            }
            else {
                if( abs(mas_of_temp[j]) < min_p) {
                    min_p = abs(mas_of_temp[j]);
                }
            }
        }
        if(min_n < min_p) {
            min = -min_n;
        }
        else {
            min = min_p;
        }
    }
    
    cout << min << endl;
    // Write an action using cout. DON'T FORGET THE "<< endl"
    // To debug: cerr << "Debug messages..." << endl;
    
}