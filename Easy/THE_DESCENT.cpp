#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
    int check[8];
    int max_sr;
    int max = 0;
    int indicate;
    while (1) {
        for (int i = 0; i < 8; i++) {
            int mountainH; // represents the height of one mountain.
            cin >> mountainH; cin.ignore();
            check[i] =  mountainH;
             if (mountainH > max) {
	    	    max = mountainH;
	    	    indicate = i;
	        }
        }
            
        for (int j = 0; j < 8; j++) {   
    	    if (check[j] > max) {
	    	    max = check[j];
	    	    indicate = j;
	        }
        }
	    cout << indicate << endl;
	    max = 0;
    }
}