#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
    const int bit = 7;
    string MESSAGE;
    getline(cin, MESSAGE);
    
    int size_string = MESSAGE.size();
    vector<int> value;
    
    string binary;
    string::iterator iter;
    
    for(int i = 0; i < size_string; i++) {
        value.push_back((int)MESSAGE[i]);
        int j = 0;
        string buff;
        buff.resize(bit);
        while(j != bit) {
            if(value[i] % 2 == 1) {
                buff.insert((bit - 1) - j, "1");
                value[i] /=2;
                j++;
            }
            else {
                buff.insert((bit - 1) - j, "0");
                value[i] /=2;
                j++;
            }
        }
        for(iter = buff.begin(); iter != buff.end(); iter++) {
            binary.push_back(*iter);
        }
    }
    
    string one = "0 ";
    string zero = "00 ";
    string encode;
    int j = 0;
    bool enter_one = false;
    bool enter_zero = false;
     
    for (iter = binary.begin(); iter != binary.end(); iter++) {
        if(*iter == '0') {
            if(enter_zero == false) {
                encode.insert(j, zero);
                j+=3;
                enter_zero = true;
            }
            if(enter_zero == true) {
                encode.insert(j, "0");
                j++;
                if(*(iter+2) == '1') {
                    encode.insert(j, " ");
                    enter_zero = false;
                    j++;
                }
            }
        }
        if(*iter == '1') {
            if(enter_one == false) {
                encode.insert(j, one);
                j+=2;
                enter_one = true;
            }
            if(enter_one == true) {
                encode.insert(j, "0");
                j++;
                if(*(iter+2) == '0') {
                    encode.insert(j, " ");
                    enter_one = false;
                    j++;
                }
            }
        }
    }
        
    cout << encode << endl;
    
    
    return 0;
}