#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <map>
#include <cctype>

using namespace std;

int main() {
    int N; // Number of elements which make up the association table.
    cin >> N; cin.ignore();
    
    int Q; // Number Q of file names to be analyzed.
    cin >> Q; cin.ignore();
    
    map<string, string> mapTable;
    
    for (int i = 0; i < N; i++) {
        string EXT; // file extension
        string MT; // MIME type.
        int pos;
        cin >> EXT >> MT; cin.ignore();
        string::iterator iter;
        for(iter = EXT.begin(); iter != EXT.end(); iter++) {
            if(islower(*iter) == 0) {
                pos = iter - EXT.begin();
                *iter = tolower(*iter);
                EXT.replace(pos, 1, 1, *iter);
            }
        }
        mapTable[EXT] = MT;
    }
    
    for (int i = 0; i < Q; i++) {
        string FNAME; // One file name per line.
        getline(cin, FNAME);
        string MT;
        string EXT;
        int pos = 0;
        
        string::reverse_iterator revit;
        for(revit = FNAME.rbegin(); revit != FNAME.rend(); revit++) {
            if(*revit == '.') {
                pos = revit - FNAME.rbegin();
                break;
            }
        }
        
        if(pos != 0) {
            for(revit = FNAME.rbegin() + pos-1; revit >= FNAME.rbegin(); revit--) {
                EXT.push_back(*revit);
            }
            
            string::iterator iter;
            for(iter = EXT.begin(); iter != EXT.end(); iter++) {
                if(islower(*iter) == 0) {
                    pos = iter - EXT.begin();
                    *iter = tolower(*iter);
                    EXT.replace(pos, 1, 1, *iter);
                }
            }
            
            MT = mapTable[EXT];
            
            if(MT.size() == 0) {
                cout << "UNKNOWN" << endl;
                pos = 0;
            }
            else {
                cout << MT << endl;
                pos = 0;
            }
        }
        else {
            cout << "UNKNOWN" << endl;
        }
    }
}