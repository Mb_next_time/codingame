#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
int main() {
    int W; // width of the building.
    int H; // height of the building.
    cin >> W >> H; cin.ignore();
    int N; // maximum number of turns before game over.
    cin >> N; cin.ignore();
    int X0;
    int Y0;
    cin >> X0 >> Y0; cin.ignore();
    // game loop
    int c = 2;
    int W0 = 0;
    int H0 = 0;
    while (1) {
       
        string bombDir; 
        cin >> bombDir; cin.ignore();
        
        if (bombDir == "U") {
            H = Y0;
            Y0=(H- H0)/c+H0;
        }
        
        if (bombDir == "R") {  
            W0=X0;
            X0=(W - W0)/c+W0;
        }
        
        if (bombDir == "D") {   
            H0=Y0;
            Y0=(H - H0)/c+H0;
        }
        
        if (bombDir == "L") {
            W = X0;
            X0=(W - W0)/c+W0;
        }
        
        if (bombDir == "UR") {
            H = Y0;
            Y0=(H - H0)/c+H0;
            W0=X0;
            X0=(W - W0)/c+W0;
        }
        
        if (bombDir == "DR") {   
            H0=Y0;
            Y0=(H - H0)/c+H0;
            W0=X0;
            X0=(W - W0)/c+W0;
        }
        
        if (bombDir == "DL") {
            H0=Y0;
            Y0=(H - H0)/c+H0;
            W = X0;
            X0=(W - W0)/c+W0;
        }
        
        if (bombDir == "UL") {
            H = Y0;
            Y0=(H - H0)/c+H0;
            W = X0;
            X0=(W - W0)/c+W0;
        }
        
        cout << X0 << " " << Y0 << endl;
    }
}