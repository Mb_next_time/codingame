#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <queue>

using namespace std;

/**
* Auto-generated code below aims at helping you parse
* the standard input according to the problem statement.
**/
int main()
{
	int N; // the total number of nodes in the level, including the gateways
	int L; // the number of links
	int E; // the number of exit gateways



	cin >> N >> L >> E; cin.ignore();
	vector<int> goal(E);
	cerr << "N: " << N << endl;
	cerr << "L: " << L << endl;
	cerr << "E: " << E << endl;

	/* Initilization of graph */
	vector<vector<int> > network(N);
	for (int i = 0; i < N; i++) {
		network[i].resize(N);
		for (int j = 0; j < N; ++j)
			network[i][j] = 0;
	}

	for (int i = 0; i < L; i++) {
		int N1; // N1 and N2 defines a link between these nodes
		int N2;
		cin >> N1 >> N2; cin.ignore();
		//cerr << "Links: " << N1 << " and " << N2 << endl;
		network[N1][N2] = 1;
		network[N2][N1] = 1;
	}

	cerr << "Map" << endl;
	for (int i = 0; i < N; i++) {
		network[i].resize(N);
		for (int j = 0; j < N; ++j)
			cerr << network[i][j] << " ";
		cerr << endl;
	}

	for (int i = 0; i < E; i++) {
		int EI; // the index of a gateway node
		cin >> EI; cin.ignore();
		goal[i] = EI;
		cerr << "Exit: " << EI << endl;
		cerr << "goal[" << i << "]: " << goal[i] << endl;
	}
	// game loop
	while (1) {
		int SI; // The index of the node on which the Skynet agent is positioned this turn
		cin >> SI; cin.ignore();
		cerr << "Position of agent: " << SI << endl;
		vector<vector<bool> > used(E);
		vector<vector<int> > p(E);

		for (int k = 0; k < E; k++) {
			queue<int> q;
			q.push(goal[k]);
			used[k].resize(N);
			p[k].resize(N);
			used[k][goal[k]] = true;
			p[k][goal[k]] = -1;

			while (!q.empty()) {
				int v = q.front();
				q.pop();
				for (int i = 0; i < network[v].size(); i++) {
					int to = network[v][i];
					if (network[v][i] == 1) {
						if (!used[k][i]) {
							used[k][i] = true;
							q.push(i);
							p[k][i] = v;
						}
					}
				}
			}
		}

		vector<vector<int>> path(E);
		int len = 1000;
		int index;

		for (int i = 0; i < E; i++) {
			for (int v = SI; v != -1; v = p[i][v]) {
				path[i].push_back(v);
			}
		}

		for (int i = 0; i < E; i++) {
			if (path[i].size() < len) {
				len = path[i].size();
				index = i;
			}
		}

		for (int i = 0; i<path[index].size(); i++)
			cout << path[index][i] << " ";
		cout << endl;
	}
}