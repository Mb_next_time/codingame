﻿#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class tree {        
public:
    tree() {        //конструктор  без параметров
        root = nullptr;
    }
    tree (int value) {      // конструктор с параметрам
        root = new Node;
        root->data = value;
        root->child = new Node*[root->maxCh];
        root->parent = nullptr;
    }
    void insert(int prev, int next) {   // метод для вставки нового узла в дерево
        if (prev == root->data) {
            if(curCh == root->numberCh) {
                createNode(next);
                curCh = 0;
            }
            else {
                if(checkLink(prev, next)) {
                    curCh = 0;
                }
                else {
                    curCh++;
                    insert(prev, next);
                }
            }
        }
        else {
            root->check = true;
            if(root->leaf == false)  {
                for(int i = 0; i < root->numberCh; i++) {
                    if (root->child[i]->check == false) {
                        root = root->child[i];
                        insert(prev, next);
                    }
                }
            }
            if((root->check == true) && (root->parent != nullptr)) {
                root = root->parent;
                insert(prev, next);
            }
        }
    }
    
    void checkRes() {       // метод для сбрасывания меток check с узлов дерева
        root->check = false;
        for(int i = 0; i < root->numberCh; i++) {
            if(root->child[i]->check == true) {
                root = root->child[i];
                checkRes();
            }
        }
        if((root->check == false) && (root->parent != nullptr)) {
            root = root->parent;
            checkRes();
        }
    }
 
    void traversal() {      // обход дерева с  вычислением глубины дерева 
        root->check = true;
        for(int i = 0; i < root->numberCh; i++) {
            if(root->child[i]->check == false) {
                root = root->child[i];
                deep++;
                traversal();
            }
        }
        if(root->leaf == true) {
            if(deep > maxdeep) {
                maxdeep = deep;
            }
        }
        if((root->check == true) && (root->parent != nullptr)) {
            root = root->parent;
            deep--;
            traversal();
        }
    }
    
    int deepTree() {        // метод возвращает  максимальную глубину дерева
        return maxdeep;
    }
    ~tree() {		// деструктор		
    }
    
private:
    struct Node {               // структура узла дерева
        int data;               // данные 
        Node **child;           // указатель на  потомков
        Node *parent;           // указатель на родителя
        int numberCh = 0;       // кол-во детей  на текущий момент
        int maxCh = 3;          // кол-во максимально возможных детей
        bool check = false;     // вспомогательная переменная для проверки узлов
        bool leaf = true;       // проверка, является ли узел листвой
    };
    Node* root;                 // укащатель на корень дерева
    int curCh = 0;              // переменная  для  обращения к потомку в данный момент
    int deep = 1;               // глубина  дерева в данный момент
    int maxdeep = 0;            // максимальная глубина дерева
    
    void createNode(int value) {        // метод для создания нового узла
        Node *_node = new Node;
        _node->data = value;
        _node->child = new Node*[_node->maxCh];
        _node->parent = root;
        
        root->child[root->numberCh] = _node;
        root->numberCh++;
        root->leaf = false;
    }
    
    bool checkLink(int prev, int next) {        // метод для проверки существования связи между узлами
        
        if(root->leaf == true) {
            return false;
        }
        if ((root->data == prev) && (root->child[curCh]->data == next)) {
            return true; 
        }
        else {
            return false;
        }
    }
};
 
int main()
{
    int n; // the number of relationships of influence
    cin >> n; cin.ignore();
    
    vector<int> X;
    vector<int> Y;

    for (int i = 0; i < n; i++) {
        int x; // a relationship of influence between two people (x influences y)
        int y;
        cin >> x >> y; cin.ignore();
        X.push_back(x);
        Y.push_back(y);
    }
    vector<int> roots;
    // поиск корня
    for(int i = 0; i < X.size(); i++) {
        vector<int>::iterator it = find_if(Y.begin(), Y.end(), [&](int j) {
            return (X[i] == j);
        });
        if(!(*it)) {
            roots.push_back(X[i]);
        }
    }
    
    // убирание дубликатов в массиве корней
    vector<int>::iterator it = std::unique (roots.begin(), roots.end());
    roots.resize( distance(roots.begin(),it) ); 
    
    vector<tree> forest;
    
    for(int i = 0; i < roots.size(); i++) {
        forest.push_back(roots[i]);
    }
    
    // вставка связей  в  деревья
    int N = 3; 
    for(int i = 0; i < forest.size(); i++) {
        for(int k = 0; k < N; k++) {
            for(int j = 0; j < X.size(); j++) {
                forest[i].insert(X[j], Y[j]);
                forest[i].checkRes();
            }
        }
    }
    
    // обход деревьев
    for(int i = 0; i < forest.size(); i++) {
        forest[i].traversal();
    }
    
    int max = 0;
    for(int i = 0; i < forest.size(); i++) {
        if(forest[i].deepTree() > max) {
            max = forest[i].deepTree();
        }
    }
    cout << max << endl;
}