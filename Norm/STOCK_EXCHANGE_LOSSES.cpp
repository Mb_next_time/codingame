#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

/**
* Auto-generated code below aims at helping you parse
* the standard input according to the problem statement.
**/
int main()
{
	int n;
	cin >> n; cin.ignore();
	vector<int> data(n);

	for (int i = 0; i < n; i++) {
		int v;
		cin >> v; cin.ignore();
		data[i] = v;
	}

	int max = 0;
	int diff = 0;
	int loc_diff;

	for (int i = 0; i < n - 1; i++) {
		if (data[i] > max) {
			max = data[i];
		}
		if (data[i] < data[i + 1]) {
			loc_diff = max - data[i];
			if (loc_diff > diff) {
				diff = loc_diff;
			}
		}
		else {
			if (i == n - 2) {
				loc_diff = max - data[i + 1];
				if (loc_diff > diff) {
					diff = loc_diff;
				}
			}
		}
	}

	int zero = 0;

	cout << zero - diff << endl;

}