#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <deque>

using namespace std;

 int parser(const string &str)
 {
    vector<string> cardValue = { "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};
     
    int indexCardValue = 0;
    string val = str.substr(0, str.length() -1);
     
    while(indexCardValue != cardValue.size())
    {   
        if(cardValue[indexCardValue] == val)
        {   
            return indexCardValue;
        }
        ++indexCardValue;
    }
 }
 
 void putOutCardFromPile(deque<int> &deck, deque<int> &pile)
 {
    while(!pile.empty())
    {
        deck.push_back(pile.front());
        pile.pop_front();
    }
 }
 
 void game(deque<int> &deckP1, deque<int> &deckP2)
 {  
    int roundGame = 0;
    
    deque<int> pileP1;
    deque<int> pileP2;
    string statusGame;
 
    while(!deckP1.empty() && !deckP2.empty())
    {   
        int card1 = deckP1.front();
        deckP1.pop_front();
        
        int card2 = deckP2.front();
        deckP2.pop_front();
        
        if(card1 > card2)
        {   
            putOutCardFromPile(deckP1, pileP1);
            deckP1.push_back(card1);
            
            putOutCardFromPile(deckP1, pileP2);
            deckP1.push_back(card2);
           
            ++roundGame;
        }
        
        if(card1 < card2)
        {   
            putOutCardFromPile(deckP2, pileP1);
            deckP2.push_back(card1);
            
            putOutCardFromPile(deckP2, pileP2);
            deckP2.push_back(card2);
            
            ++roundGame;
        }
        
        // put card in pile
        if(card1 == card2)
        {   
            int numberCardInPile = 3;
            
            // if enough space
            if(deckP1.size() >= numberCardInPile + 1 && deckP2.size() >= numberCardInPile + 1)
            {   
                pileP1.push_back(card1);
                pileP2.push_back(card2);
                
                for(int i = 0; i < numberCardInPile; i++)
                {
                    pileP1.push_back(deckP1.front());
                    deckP1.pop_front();
                
                    pileP2.push_back(deckP2.front());
                    deckP2.pop_front();
                }
            }
            else
            {
                statusGame = "PAT";
                break;
            }
        }
    }
    
    if(statusGame == "PAT")
    {
        cout << statusGame << endl;
    }
    else if(deckP1.size() > deckP2.size())
    {
        cout << "1 " << roundGame << endl;
    }
    else
    {
        cout << "2 " << roundGame << endl;
    }
 }
 
int main()
{
    int n; // the number of cards for player 1
    cin >> n; cin.ignore();
    
    deque<int> deckP1;
    
    for (int i = 0; i < n; i++) {
        string cardp1; // the n cards of player 1
        cin >> cardp1; cin.ignore();
        
        deckP1.push_back(parser(cardp1));
    }
    
    deque<int> deckP2;
    int m; // the number of cards for player 2
    cin >> m; cin.ignore();
    for (int i = 0; i < m; i++) {
        string cardp2; // the m cards of player 2
        cin >> cardp2; cin.ignore();
        deckP2.push_back(parser(cardp2));
    }
    
    game(deckP1, deckP2);
}