#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
    
    int width; // the number of cells on the X axis
    cin >> width; cin.ignore();
    int height; // the number of cells on the Y axis
    cin >> height; cin.ignore();
    string *matrix;
    matrix = new string[height + 1];
    
    for (int i = 0; i < height; i++) {
        string line; // width characters, each either 0 or .
        getline(cin, line);
        matrix[i] = line + "*";
    }
    matrix[height] = "*";

    for(int i = 0; i < height + 1; i++) {
            
        string out1;  
        string out2;
        string out3;
       
        for(int j = 0; j < width + 1; j++) {
            
            if(matrix[i][j] == '*') {
                break;
            }
            
            if(matrix[i][j] == '0') {
                out1 = to_string(j) + " " + to_string(i) + " ";
            }
            else {
                continue;
            }
            
            //check rigth neighbor
            int k = j;
            while(k != width) {
                if(matrix[i][k + 1] == '0') {
                    out2 = to_string(k + 1) + " " + to_string(i) + " ";
                    break;
                }
                else {
                    if (matrix[i][k + 1] == '.') {
                        k++;
                    }
                    else {
                        string noCoor = "-1";
                        out2 = noCoor + " " + noCoor + " ";
                        break;
                    }
                }
            }
            
            //check bottom neighbor
            k = i;
            while(k != height) {
                if(matrix[k + 1][j] == '0') {
                    out3 = to_string(j) + " " + to_string(k + 1) + " ";
                    break;
                }
                else {
                    if (matrix[k + 1][j] == '.') {
                        k++;
                    }
                    else {
                        string noCoor = "-1";
                        out3 = noCoor + " " + noCoor + " ";
                        break;
                    }
                }
            }
            
            cout << out1 << out2 << out3 << endl;
        }
    }
}