#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

/**
* Auto-generated code below aims at helping you parse
* the standard input according to the problem statement.
**/
int main()
{
	int N;
	cin >> N; cin.ignore();
	cerr << "N = " << N << endl;
	int C;
	cin >> C; cin.ignore();
	cerr << "C = " << C << endl;
	vector <int> sol(N);
	vector <int> budg(N);

	for (int i = 0; i < N; i++) {
		int B;
		cin >> B; cin.ignore();
		budg[i] = B;
	}

	sort(budg.begin(), budg.begin() + N);

	for (int i = 0; i < N; i++) {
		int opt = C / (N - i);
		if (budg[i] >= opt) {
			C -= opt;
			sol[i] = opt;
		}
		else {
			C -= budg[i];
			sol[i] = budg[i];
		}
	}

	if (C != 0) {
		cout << "IMPOSSIBLE" << endl;
	}
	else {
		for (int i = 0; i < N; i++) {
			cout << sol[i] << endl;
		}
	}
}