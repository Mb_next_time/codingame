#include <iostream>
#include <algorithm>
#include <list>
#include <iterator> 

using namespace std;

/**
* Auto-generated code below aims at helping you parse
* the standard input according to the problem statement.
**/
int main()
{
	int R;
	cin >> R; cin.ignore();
	int L;
	cin >> L; cin.ignore();

	list<int> seqN;
	seqN.push_back(R);

	int count = 0;
	int n = 1;

	while (n < L) {

		list<int> seqOl;
		seqOl = seqN;

		seqN.clear();

		auto iter = seqOl.begin();

		while (iter != seqOl.end()) {
			int var = *iter;
			auto after = next(iter, 1);
			if (*iter == *after) {
				while ((*iter == *after) && (iter != seqOl.end())) {
					count++;
					iter++;
				}
			}
			else {
				count = 1;
				iter++;
			}
			seqN.push_back(count);
			seqN.push_back(var);
			count = 0;
		}
		n++;
	}

	for (auto iter = seqN.begin(); iter != seqN.end(); iter++) {
		auto after = next(iter, 1);
		if (after == seqN.end()) {
			cout << *iter << endl;
		}
		else {
			cout << *iter << " ";
		}
	}
}