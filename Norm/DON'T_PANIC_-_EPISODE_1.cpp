#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <map>

using namespace std;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
int main()
{
    int nbFloors; // number of floors
    int width; // width of the area
    int nbRounds; // maximum number of rounds
    int exitFloor; // floor on which the exit is found
    int exitPos; // position of the exit on its floor
    int nbTotalClones; // number of generated clones
    int nbAdditionalElevators; // ignore (always zero)
    int nbElevators; // number of elevators
    cin >> nbFloors >> width >> nbRounds >> exitFloor >> exitPos >> nbTotalClones >> nbAdditionalElevators >> nbElevators; cin.ignore();
    // cout << nbRounds << endl;
    // cout << exitFloor << " " << exitPos << endl;
   
    map<int, int> el_Pos_Fl;
    
    for (int i = 0; i < nbElevators; i++) {
        int elevatorFloor; // floor on which this elevator is found
        int elevatorPos; // position of the elevator on its floor
        cin >> elevatorFloor >> elevatorPos; cin.ignore();
        el_Pos_Fl[elevatorFloor] = elevatorPos;
    }

    // game loop
    // cout <<  el_Pos_Fl[3] << endl;
    bool lock = false;
    int shift = 0;
    int count = 0;
    
    while (1) {
    // for(int i = 0; i < 25; i++) {
        int cloneFloor; // floor of the leading clone
        int clonePos; // position of the leading clone on its floor
        string direction; // direction of the leading clone: LEFT or RIGHT
       
        cin >> cloneFloor >> clonePos >> direction; cin.ignore();
        
        if(clonePos == el_Pos_Fl[cloneFloor]) {
            lock = true;
        }
        
        if(direction == "RIGHT") {
            
            if(clonePos == width - 1) {
                cout << "BLOCK" << endl;
                continue;
            }
            
            if(cloneFloor == exitFloor) {
                if(clonePos - exitPos  < 0) {
                    if((clonePos == el_Pos_Fl[exitFloor-1] - 1) && (lock)) {
                        cout << "BLOCK" << endl;
                        lock = false;
                        continue;
                    }
                }  
                if(clonePos - exitPos  > 0) {
                    if((clonePos == el_Pos_Fl[exitFloor-1] + 1) && (lock)) {
                        cout << "BLOCK" << endl;
                        lock = false;
                        continue;
                    }
                }
            }
            else {
                if (clonePos - el_Pos_Fl[cloneFloor]  < 0){
                    if((clonePos == el_Pos_Fl[cloneFloor-1] - 1) && (lock)) {
                        cout << "BLOCK" << endl;
                        lock = false;
                        continue;
                    }
                }
                if(clonePos - el_Pos_Fl[cloneFloor]  > 0) {
                    if((clonePos  == el_Pos_Fl[cloneFloor-1] + 1) && (lock)) {
                        // cout << clonePos << endl;
                        cout << "BLOCK" << endl;
                        lock = false;
                        continue;
                    }
                } 
            } 
        }
        
        if(direction == "LEFT") {
            if(clonePos == 0) {
                cout << "BLOCK" << endl;
                continue;
            }
            if(cloneFloor == exitFloor) {
                if(clonePos - exitPos  < 0) {
                    if((clonePos == el_Pos_Fl[exitFloor-1] - 1) && (lock)) {
                        cout << "BLOCK" << endl;
                        lock = false;
                        continue;
                    }
                }  
                if (clonePos - exitPos  > 0) {
                    if((clonePos == el_Pos_Fl[exitFloor-1] + 1) && (lock)) {
                        cout << "BLOCK" << endl;
                        lock = false;
                        continue;
                    }
                } 
            }
            else {
                if (clonePos - el_Pos_Fl[cloneFloor]  < 0) {
                    if((clonePos == el_Pos_Fl[cloneFloor-1] - 1) && (lock)) {
                        cout << "BLOCK" << endl;
                        lock = false;
                        continue;
                    }
                }
                if(clonePos - el_Pos_Fl[cloneFloor]  > 0) {
                    if((clonePos  == el_Pos_Fl[cloneFloor-1] + 1) && (lock)) {
                        cout << "BLOCK" << endl;
                        lock = false;
                        continue;
                    }
                } 
            }
        }
        cout << "WAIT" << endl;  
    }
}