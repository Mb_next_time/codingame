#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class robot
{
public:     
    robot() { map = nullptr; }
    robot(string **_map, int _L, int _C)
    {
        map = _map;
        L = _L;
        C = _C;
        int maxSteps = (L-2)*(C-2);
        findStart();
        while(map[x][y] != end)
        {   
            if(numberIteration > maxSteps)
            {   
                path.clear();
                path.push_back(LOOP);
                break;
            }
            while(findObstacle())
                changeCurrentDirection();
            
            move();
            takeModifier();
            takeBeer();
            takeInverter();
            takeTeleport();
            breakObstacle();
            ++numberIteration;
        }
    }
    void displayPath()
    {
        for(auto it = path.begin(); it != path.end(); ++it)
            cout << *it << endl;
    }
    ~robot() { delete[] map; }
private:
    string **map;
    vector<string> directions{"SOUTH", "EAST", "NORTH", "WEST"};
    vector<string> path;
    bool blockSouth   = false;
    bool blockEast    = false;
    bool blockNorth   = false;
    bool blockWest    = false;
    bool modeBreaker  = false;
    bool modeInvertor = false;
    unsigned short currentDirectionIndex = 0;
    int unsigned numberIteration = 0;
    int x;
    int y;
    int L;
    int C;
    string border     = "#";
    string obstacle   = "X";
    string start      = "@";
    string end        = "$";
    string south      = "S";
    string east       = "E";
    string north      = "N";
    string west       = "W";
    string beer       = "B";
    string invertor   = "I";
    string teleport   = "T";
    string LOOP       = "LOOP";
    string empty      = " ";
    void changeCurrentDirection()
    {   
        if(!modeInvertor)
        {
            if(!blockSouth)
            {
                currentDirectionIndex = 0;
                blockSouth = true;
                return;
            }
            
            if(!blockEast)
            {
                currentDirectionIndex = 1;
                blockEast = true;
                return;
            }
            
            if(!blockNorth)
            {
                currentDirectionIndex = 2;
                blockNorth = true;
                return;
            }
            
            if(!blockWest)
            {
                currentDirectionIndex = 3;
                blockWest = true;
                return;
            }
        }
        
        if(modeInvertor)
        {   
            if(!blockWest)
            {
                currentDirectionIndex = 3;
                blockWest = true;
                return;
            }
            if(!blockNorth)
            {
                currentDirectionIndex = 2;
                blockNorth = true;
                return;
            }
            
            if(!blockEast)
            {
                currentDirectionIndex = 1;
                blockEast = true;
                return;
            }
            
            if(!blockSouth)
            {
                currentDirectionIndex = 0;
                blockSouth = true;
                return;
            }
        }
    }
    void move()
    {   
        incrCoordinate(x, y);
        path.push_back(directions[currentDirectionIndex]);
        blockSouth = false;
        blockEast  = false;
        blockNorth = false;
        blockWest  = false;
    }
    bool findObstacle()
    {   
        int analysingSpaceX = x;
        int analysingSpaceY = y;
        
        incrCoordinate(analysingSpaceX, analysingSpaceY);
        
        if(map[analysingSpaceX][analysingSpaceY] == border || (map[analysingSpaceX][analysingSpaceY] == obstacle && !modeBreaker))
            return true;
            
        return false;
    }
    void findStart()
    {
        for(int i = 1; i < L-1; ++i)
        {
            for(int j = 1; j < C-1; ++j)
            {
                if(map[i][j] == start)
                {   
                    x = i;
                    y = j;
                    return;
                }
            }
        }
    }
    void incrCoordinate(int& _x, int& _y)
    {
        switch(currentDirectionIndex)
        {
            case 0:
                ++_x;
                break;
            case 1:
                ++_y;
                break;
            case 2:
                --_x;
                break;
            case 3:
                --_y;
                break;
        } 
    }
    void takeModifier()
    {
        if(map[x][y] == south)
        {
            currentDirectionIndex = 0;
            return;
        }
            
        if(map[x][y] == east)
        {
            currentDirectionIndex = 1;
            return;
        }
        
        if(map[x][y] == north)
        {
            currentDirectionIndex = 2;
            return;
        }
        
        if(map[x][y] == west)
        {
            currentDirectionIndex = 3;
            return;
        }
    }
    void takeBeer()
    {
        if(map[x][y] == beer && !modeBreaker)
            modeBreaker = true;
        else if(map[x][y] == beer && modeBreaker) 
            modeBreaker = false;
    }
    void breakObstacle()
    {
        if(map[x][y] == obstacle && modeBreaker)
            map[x][y] = empty;
    }
    void takeInverter()
    {
        if(map[x][y] == invertor && !modeInvertor)
            modeInvertor = true;
        else if(map[x][y] == invertor && modeInvertor) 
            modeInvertor = false;
    }
    void takeTeleport()
    {
        if(map[x][y] == teleport)
        {   
            for(int i = 1; i < L - 1; ++i)
            {
                for(int j = 1; j < C - 1; ++j)
                {   
                    if(map[i][j] == teleport && (i != x || j != y)) 
                    {   
                        x = i;
                        y = j;
                        return;
                    }
                }
            }
        }
    }
};

int main()
{
    int L;
    int C;
    cin >> L >> C; cin.ignore();
    string **map = nullptr;
    
    map = new string*[L];
    for(int i = 0; i < L; i++)
        map[i] = new string[C];
    
    for (int i = 0; i < L; i++) {
        string row;
        getline(cin, row);
        for(int j = 0; j < row.size(); ++j)
            map[i][j] = row[j];
    }
    
    robot Bender(map, L, C);
    Bender.displayPath();
    
    for(int i = 0; i < L; ++i)
        delete[] map[i];
        
    delete map;
}