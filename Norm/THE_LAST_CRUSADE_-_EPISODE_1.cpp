#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
int main()
{
    int W; // number of columns.
    int H; // number of rows.
    cin >> W >> H; cin.ignore();
    vector<vector<int>> blocks(H);
    
    for(int i = 0; i < H; i++) {
        blocks[i].resize(W);
    }
    
    string buf;
    int st = 0;
    int fin = 0;
    int k = 0;
    
    for (int i = 0; i < H; i++) {
        string LINE; // represents a line in the grid and contains W integers. Each integer represents one room of a given type.
        getline(cin, LINE);
        
        for(int j = 0; j < LINE.length(); j++) {
            st = j;
            while (1) {
                fin = j;
                if(LINE[j] == ' ' || LINE[j] == '\0')
                    break;
                else 
                    j++;
            }
            buf = LINE.substr(st, fin - st);
            blocks[i][k] = stoi(buf);
            k++;
        }
        k = 0;
    }
    int EX; // the coordinate along the X axis of the exit (not useful for this first mission, but must be read).
    cin >> EX; cin.ignore();
    
    // game loop
    while (1) {
        int XI;
        int YI;
        string POS;
        cin >> XI >> YI >> POS; cin.ignore();
        
        // Write an action using cout. DON'T FORGET THE "<< endl"
        // To debug: cerr << "Debug messages..." << endl;
        if(blocks[YI][XI] == 1 || blocks[YI][XI] == 3 || (blocks[YI][XI] == 4 && POS == "RIGHT") ||
                    (blocks[YI][XI] == 5 && POS == "LEFT") ||  blocks[YI][XI] == 7 || blocks[YI][XI] == 8 || 
                        blocks[YI][XI] == 9 || blocks[YI][XI] == 12 || blocks[YI][XI] == 13 ) {
            YI += 1;
            cout << XI << " " << YI << endl;
            continue;
        }
        
        if((blocks[YI][XI] == 2 && POS == "RIGHT") || (blocks[YI][XI] == 4 && POS == "TOP") || 
            (blocks[YI][XI] == 6 && POS == "RIGHT") || blocks[YI][XI] == 10) {
            XI -= 1;
            cout << XI << " " << YI << endl;
            continue;
        }
        
        if((blocks[YI][XI] == 2 && POS == "LEFT") || (blocks[YI][XI] == 5 && POS == "TOP") || (blocks[YI][XI] == 6 && POS == "LEFT") ||
                blocks[YI][XI] == 11) {
            XI += 1;
            cout << XI << " " << YI << endl;
            continue;
        }
    }
}